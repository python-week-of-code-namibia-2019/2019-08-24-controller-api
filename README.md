# Python Week Of Code, Namibia 2019

## Task for Instructor

We want a new view to return the post's title as JSON.

1. Add the URL

   ```
   path('api/blog/<id>/', views.api_blog, name='api_blog')
   ```

   to in [blog/urls.py](blog/urls.py).
2. Create the function `api_blog` in [blog/views.py](blog/views.py).

   ```
   from django.http import JsonResponse

   def api_blog(request, id):
       posts = get_list_or_404(
           Post,
           id=id
       )

       return JsonResponse(
           [{"title": post.title} for post in posts],
           safe=False
       )
   ```
3. Access http://localhost:8000/api/blog/1/ with your web browser.
4. Run

   ```
   curl http://localhost:8000/api/blog/1/
   ```

   from your **UNIX-like** command line.

## Tasks for Learners

1. Change the function `api_blog` to also return the `text`.

## Extra

[Django REST framework](https://www.django-rest-framework.org/) is a powerful and flexible toolkit for building Web APIs.
We recommend to use it.