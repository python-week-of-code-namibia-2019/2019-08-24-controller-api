from datetime import datetime

from django.http import JsonResponse
from django.shortcuts import get_object_or_404, get_list_or_404, render

from .models import Post

# Create your views here.
def index(request):
    posts = Post.objects.all()

    return render(
        request,
        'blog/index.html',
        {
            'posts': posts,
        }
    )

def month_archive(request, year, month):
    posts = get_list_or_404(
        Post,
        published_date__gte=datetime(year, month, 1),
        published_date__lte=datetime(year, month, 31),
    )

    return render(
        request,
        'blog/month.html',
        {
            "year": year,
            "month": month,
            "posts": posts,
        }
    )
    

def blog(request, title):
    post = get_object_or_404(
        Post,
        title__iexact=title
    )

    return render(
        request,
        'blog/post.html',
        {
            "post": post,
        }
    )


# API

def api_blog(request, id):
    posts = get_list_or_404(
        Post,
        id=id
    )

    return JsonResponse(
        [{"title": post.title} for post in posts],
        safe=False
    )