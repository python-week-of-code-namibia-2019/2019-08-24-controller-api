from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('blog/<int:year>/<int:month>/', views.month_archive, name="blog_month_archive"),
    path('blog/<title>/', views.blog, name='blog'),
    path('api/blog/<id>/', views.api_blog, name='api_blog')
]
